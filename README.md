# Spell That Bot

AWS lambda Telegram bot spells by NATO phonetic alphabet.

[Icons](https://freeicons.io/icon-list/regular-life-icons) made by [Anu Rocks](https://freeicons.io/profile/730)
from www.freeicons.io.

## Installation

### Prerequisites

-   JDK 11+
-   Maven 3.6.0+
-   AWS Lambda, AWS API Gateway

### Build and test

Just run

    mvn clean verify

### AWS Lambda

For version 1 you should set environment variables:

-   ICON_PATH, see below
-   SPELLERS, value `English,Phone number`
-   SPELL_THAT_BOT_LOG_LEVEL if you want to override default `INFO` level

The bot uses icons for answer on inline queries: `chat` for options, `error` if any speller could not spell text, `info` that is
shown for empty queries. The icon path should be String's format string with `%s` for icon name, e. g. `https://example.com/icons/%s.png`

### Deploy to AWS

You can upload the code manually or use **aws-maven-plugin**

#### AWS user to upload the function

Create AWS user with permission to update function:

    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "VisualEditor0",
                "Effect": "Allow",
                "Action": [
                    "lambda:UpdateFunctionCode",
                    "lambda:InvokeFunction",
                    "lambda:InvokeAsync"
                ],
                "Resource": "arn:aws:lambda:<your region>:<your AWS user id>:function:<function name>"
            }
        ]
    }

#### Upload the function

First read [how to set AWS credentials](https://github.com/davidmoten/aws-maven-plugin#deploy-to-lambda) for
**aws-maven-plugin**. Use Maven property `lambda.name` to override lambda's name for your own, default value is `spell-that-bot`.

Then run

    mvn aws:deployLambda

## Usage

### Private chat with the bot

It uses default speller (English for version 1). Type any email, login, phone number or just phrase with latin letters and the bot
spells it.

### Inline

Type in any chat name of your bot (e. g. my bot is [@spell_that_bot](https://t.me/spell_that_bot)) then type what you want to
spell. The bot shows available options or error message if any speller could not spell text.

## Contributing

Please read [CONTRIBUTING](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## History

See [CHANGELOG](CHANGELOG.md)

## Credits

TODO: Write credits

## License

[Apache License v2.0](LICENSE)
[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat)](http://www.apache.org/licenses/LICENSE-2.0.html)
