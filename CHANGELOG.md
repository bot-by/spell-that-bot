# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased

## [1.0.0](https://gitlab.com/bot-by/spell-that-bot/-/tags/1.0.0) - 2020-12-17
### Added

-   Spell or English (Latin) word or phrase, phone number.

-   Work in private chat and inline.

-   Localisation, messages and help pages: Belorussian, Catalan, Dutch, English (default), French, German, Italian, Polish,
    Portuguese, Romanian, Russian, Spanish, Turkish, Ukrainian.
