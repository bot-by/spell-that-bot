package uk.bot_by.spell_that.telegram;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("fast")
class TelegramUtilsTest {

	@DisplayName("Clear spelled text")
	@Test
	public void clearSpelledText() {
		// given
		JSONArray entities = new JSONArray();
		JSONObject cashtag = new JSONObject();
		String cashtagEntityText = "$USD";
		String textWithEntities = "test $USD message";

		cashtag.put("length", cashtagEntityText.length());
		cashtag.put("offset", textWithEntities.indexOf(cashtagEntityText));
		cashtag.put("type", "cashtag");
		entities.put(cashtag);

		// when
		String clearText = TelegramUtils.clearSpelledText(textWithEntities, entities);

		// then
		assertEquals("test      message", clearText);
	}

	@DisplayName("Webhook: answer inline query")
	@Test
	public void webhookAnswerInlineQuery() {
		// given
		Map<String, String> messageMap = new HashMap<>();

		messageMap.put("first title", "first message");
		messageMap.put("second title", "second message");

		// when
		String webhookMessage = TelegramUtils.makeWebhookAnswerInlineQuery(4321, messageMap);

		// then
		JSONObject message = assertDoesNotThrow(() -> new JSONObject(new JSONTokener(webhookMessage)), "JSON is well formed");

		assertAll("Webhook answerInlineQuery (for empty query)",
				() -> assertThat(message.keySet(),
						containsInAnyOrder("method", "inline_query_id", "results", "is_personal")),
				() -> assertEquals("answerInlineQuery", message.getString("method")),
				() -> assertEquals(4321L, message.getLong("inline_query_id")),
				() -> assertDoesNotThrow(() -> message.getBoolean("is_personal"), "Is personal"));

		JSONArray results = assertDoesNotThrow(() -> message.getJSONArray("results"), "Results is an array");

		assertEquals(2, results.length(), "Results count");

		Set<String> titles = new HashSet<>(2), messages = new HashSet<>(2);

		titles.add("first title");
		titles.add("second title");
		messages.add("first message");
		messages.add("second message");

		assertAll("Results",
				() -> assertThat(results.getJSONObject(0).keySet(),
						containsInAnyOrder("input_message_content", "title", "id", "type", "thumb_url", "thumb_width",
								"thumb_height")),
				() -> assertEquals("article", results.getJSONObject(0).getString("type")),
				() -> assertNotNull(results.getJSONObject(0).getString("id"), "Result 1 Id"),
				() -> assertTrue(titles.remove(results.getJSONObject(0).getString("title")), "Title 1"),
				() -> assertDoesNotThrow(() -> results.getJSONObject(0).getJSONObject("input_message_content"),
						"Message content 1 is an object"),
				() -> assertThat(results.getJSONObject(0).getJSONObject("input_message_content").keySet(),
						containsInAnyOrder("parse_mode", "message_text")),
				() -> assertEquals("MarkdownV2",
						results.getJSONObject(0).getJSONObject("input_message_content").getString("parse_mode")),
				() -> assertTrue(messages.remove(
						results.getJSONObject(0).getJSONObject("input_message_content").getString("message_text")),
						"Message 1"),
				() -> assertEquals("http://example.com/icon/chat.png",
						results.getJSONObject(0).getString("thumb_url"), "Icon 1"),
				() -> assertThat(results.getJSONObject(1).keySet(),
						containsInAnyOrder("input_message_content", "title", "id", "type", "thumb_url", "thumb_width",
								"thumb_height")),
				() -> assertEquals("article", results.getJSONObject(1).getString("type")),
				() -> assertNotNull(results.getJSONObject(1).getString("id"), "Result 2 Id"),
				() -> assertTrue(titles.remove(results.getJSONObject(1).getString("title")), "Title 2"),
				() -> assertDoesNotThrow(() -> results.getJSONObject(1).getJSONObject("input_message_content"),
						"Message content 2 is an object"),
				() -> assertThat(results.getJSONObject(1).getJSONObject("input_message_content").keySet(),
						containsInAnyOrder("parse_mode", "message_text")),
				() -> assertEquals("MarkdownV2",
						results.getJSONObject(1).getJSONObject("input_message_content").getString("parse_mode")),
				() -> assertTrue(messages.remove(
						results.getJSONObject(1).getJSONObject("input_message_content").getString("message_text")),
						"Message 2"),
				() -> assertEquals("http://example.com/icon/chat.png",
						results.getJSONObject(0).getString("thumb_url"), "Icon 2"));
	}

	@DisplayName("Webhook: answer inline query (empty query)")
	@Test
	public void webhookAnswerInlineQueryEmpty() {
		// when
		String webhookMessage = TelegramUtils.makeWebhookAnswerInlineQuery(4321, "empty title", "empty message");

		// then
		JSONObject message = assertDoesNotThrow(() -> new JSONObject(new JSONTokener(webhookMessage)), "JSON is well formed");

		assertAll("Webhook answerInlineQuery (for empty query)",
				() -> assertThat(message.keySet(),
						containsInAnyOrder("method", "inline_query_id", "results", "cache_time", "is_personal")),
				() -> assertEquals("answerInlineQuery", message.getString("method")),
				() -> assertEquals(4321L, message.getLong("inline_query_id")),
				() -> assertDoesNotThrow(() -> message.getLong("cache_time"), "Cache time"),
				() -> assertDoesNotThrow(() -> message.getBoolean("is_personal"), "Is personal"));

		JSONArray results = assertDoesNotThrow(() -> message.getJSONArray("results"), "Results is an array");

		assertAll("Results",
				() -> assertEquals(1, results.length()),
				() -> assertThat(results.getJSONObject(0).keySet(),
						containsInAnyOrder("input_message_content", "title", "id", "type", "thumb_url", "thumb_width",
								"thumb_height")),
				() -> assertEquals("article", results.getJSONObject(0).getString("type")),
				() -> assertNotNull(results.getJSONObject(0).getString("id"), "Result Id"),
				() -> assertEquals("empty title", results.getJSONObject(0).getString("title")),
				() -> assertEquals("http://example.com/icon/info.png",
						results.getJSONObject(0).getString("thumb_url"), "Icon"));

		JSONObject messageContent = assertDoesNotThrow(() -> results.getJSONObject(0).getJSONObject("input_message_content"),
				"Message content is an object");

		assertAll("Message content",
				() -> assertThat(messageContent.keySet(), containsInAnyOrder("parse_mode", "message_text")),
				() -> assertEquals("MarkdownV2", messageContent.getString("parse_mode")),
				() -> assertEquals("empty message", messageContent.getString("message_text")));
	}

	@DisplayName("Webhook: answer inline query (empty results with description)")
	@Test
	public void webhookAnswerInlineQueryEmptyWithDescription() {
		// when
		String webhookMessage = TelegramUtils.makeWebhookAnswerInlineQuery(4321, "empty title", "empty message",
				"empty description");

		// then
		JSONObject message = assertDoesNotThrow(() -> new JSONObject(new JSONTokener(webhookMessage)), "JSON is well formed");

		assertAll("Webhook answerInlineQuery (for empty query)",
				() -> assertThat(message.keySet(),
						containsInAnyOrder("method", "inline_query_id", "results", "cache_time", "is_personal")),
				() -> assertEquals("answerInlineQuery", message.getString("method")),
				() -> assertEquals(4321L, message.getLong("inline_query_id")),
				() -> assertDoesNotThrow(() -> message.getLong("cache_time"), "Cache time"),
				() -> assertDoesNotThrow(() -> message.getBoolean("is_personal"), "Is personal"));

		JSONArray results = assertDoesNotThrow(() -> message.getJSONArray("results"), "Results is an array");

		assertAll("Results",
				() -> assertEquals(1, results.length()),
				() -> assertThat(results.getJSONObject(0).keySet(),
						containsInAnyOrder("input_message_content", "title", "id", "type", "description", "thumb_url",
								"thumb_width", "thumb_height")),
				() -> assertEquals("article", results.getJSONObject(0).getString("type")),
				() -> assertNotNull(results.getJSONObject(0).getString("id"), "Result Id"),
				() -> assertEquals("empty title", results.getJSONObject(0).getString("title")),
				() -> assertEquals("empty description", results.getJSONObject(0).getString("description")),
				() -> assertEquals("http://example.com/icon/error.png",
						results.getJSONObject(0).getString("thumb_url"), "Icon"));

		JSONObject messageContent = assertDoesNotThrow(() -> results.getJSONObject(0).getJSONObject("input_message_content"),
				"Message content is an object");

		assertAll("Message content",
				() -> assertThat(messageContent.keySet(), containsInAnyOrder("parse_mode", "message_text")),
				() -> assertEquals("MarkdownV2", messageContent.getString("parse_mode")),
				() -> assertEquals("empty message", messageContent.getString("message_text")));
	}

	@DisplayName("Webhook: send message")
	@Test
	public void webhookSendMessage() {
		// given
		long chatId = 12345;
		String text = "test message";

		// when
		String webhookMessage = TelegramUtils.makeWebhookSendMessage(chatId, text);

		// then
		JSONObject message = assertDoesNotThrow(() -> new JSONObject(new JSONTokener(webhookMessage)), "JSON is well formed");

		assertAll("Webhook sendMessage response",
				() -> assertThat(message.keySet(),
						containsInAnyOrder("parse_mode", "disable_web_page_preview", "method", "chat_id", "text")),
				() -> assertEquals("sendMessage", message.getString("method")),
				() -> assertEquals(12345L, message.getLong("chat_id")),
				() -> assertEquals("test message", message.getString("text")),
				() -> assertEquals("MarkdownV2", message.getString("parse_mode")),
				() -> assertTrue(message.getBoolean("disable_web_page_preview")));
	}


	@DisplayName("Webhook: send message with preview")
	@Test
	public void webhookSendMessageWithPreview() {
		// given
		long chatId = 12345;
		String text = "test message";

		// when
		String webhookMessage = TelegramUtils.makeWebhookSendMessageWithPreview(chatId, text);

		// then
		JSONObject message = assertDoesNotThrow(() -> new JSONObject(new JSONTokener(webhookMessage)), "JSON is well formed");

		assertAll("Webhook sendMessage response",
				() -> assertThat(message.keySet(),
						containsInAnyOrder("parse_mode", "disable_web_page_preview", "method", "chat_id", "text")),
				() -> assertEquals("sendMessage", message.getString("method")),
				() -> assertEquals(12345L, message.getLong("chat_id")),
				() -> assertEquals("test message", message.getString("text")),
				() -> assertEquals("MarkdownV2", message.getString("parse_mode")),
				() -> assertFalse(message.getBoolean("disable_web_page_preview")));
	}

}