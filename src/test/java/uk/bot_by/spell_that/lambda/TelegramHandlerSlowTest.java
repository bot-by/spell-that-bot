package uk.bot_by.spell_that.lambda;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Optional;

import static co.unruly.matchers.OptionalMatchers.empty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.jupiter.api.Assertions.assertAll;

@Tag("slow")
class TelegramHandlerSlowTest {

	private TelegramHandler lambdaHandler;

	@DisplayName("Start command")
	@ParameterizedTest(name = "Welcome message {argumentsWithNames}")
	@ValueSource(strings = {"en", "uk", "pl"})
	public void startCommand(String languageCode) {
		// given
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), startCommand = new JSONObject();

		chat.put("id", 4321L);
		startCommand.put("length", "/start".length());
		startCommand.put("offset", 0);
		startCommand.put("type", "bot_command");
		entities.put(startCommand);
		from.put("language_code", languageCode);
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", "/start");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertAll("Welcome message, locale " + languageCode,
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(),
						containsString("\"text\":\"welcome " + languageCode + "\"")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"disable_web_page_preview\":true")));
	}

	@DisplayName("Help command")
	@ParameterizedTest(name = "Welcome message {argumentsWithNames}")
	@ValueSource(strings = {"en", "uk", "pl"})
	public void helpCommand(String languageCode) {
		// given
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), startCommand = new JSONObject();

		chat.put("id", 4321L);
		startCommand.put("length", "/start".length());
		startCommand.put("offset", 0);
		startCommand.put("type", "bot_command");
		entities.put(startCommand);
		from.put("language_code", languageCode);
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", "/help");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertAll("Welcome message, locale " + languageCode,
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(),
						containsString("\"text\":\"[help " + languageCode + "](https://help." + languageCode + ")\"")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"disable_web_page_preview\":false")));
	}

	@DisplayName("Spell command")
	@Test
	public void spellCommand() {
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), command = new JSONObject(), from = new JSONObject(), message = new JSONObject(),
				cashtag = new JSONObject();

		chat.put("id", 4321L);
		cashtag.put("length", "$USD".length());
		cashtag.put("offset", "/spell test $USD message".indexOf("$USD"));
		cashtag.put("type", "cashtag");
		command.put("length", "/spell".length());
		command.put("offset", 0);
		command.put("type", "bot_command");
		entities.put(cashtag);
		entities.put(command);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", "/spell test $USD message");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"text\":\"6 2 5 6 0 4 2 5 5 1 3 2\"")));
	}

	@DisplayName("Spell text message with entities")
	@Test
	public void complexTextMessage() {
		// given
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), cashtag = new JSONObject();

		chat.put("id", 4321L);
		cashtag.put("length", "$USD".length());
		cashtag.put("offset", "test $USD message".indexOf("$USD"));
		cashtag.put("type", "cashtag");
		entities.put(cashtag);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", "test $USD message");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"text\":\"6 2 5 6 0 4 2 5 5 1 3 2\"")));
	}

	@DisplayName("Speller could not spell simple text message")
	@Test
	public void complexTextMessageIsNotSpelled() {
		// given
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), cashtag = new JSONObject();

		chat.put("id", 4321L);
		cashtag.put("length", "$USD".length());
		cashtag.put("offset", "bad test $USD message".indexOf("$USD"));
		cashtag.put("type", "cashtag");
		entities.put(cashtag);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", "test $USD message");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"text\":\"wrong letters\"")));
	}

	@DisplayName("Empty inline query")
	@Test
	public void emptyInlineQuery() {
		// given
		JSONObject from = new JSONObject(), message = new JSONObject();

		from.put("language_code", "en");
		message.put("from", from);
		message.put("id", 4321);
		message.put("query", "");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processInlineQuery(message);

		// then
		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"inline_query_id\":4321")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"message_text\":\"empty query\"")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"title\":\"type something\"")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"type\":\"article\"")),
				() -> assertThat(responseEvent.get().getBody(),
						containsString("\"thumb_url\":\"http://example.com/icon/info.png\"")));
	}

	@DisplayName("Inline query has phrase that could be spelled")
	@Test
	public void inlineAnswerIfSpellerCouldSpellPhrase() {
		// given
		JSONObject from = new JSONObject(), message = new JSONObject();

		from.put("language_code", "en");
		message.put("from", from);
		message.put("id", 4321);
		message.put("query", "test");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processInlineQuery(message);

		// then
		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"inline_query_id\":4321")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"message_text\":\"6 2 5 6\"")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"title\":\"test1\"")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"type\":\"article\"")),
				() -> assertThat(responseEvent.get().getBody(),
						containsString("\"thumb_url\":\"http://example.com/icon/chat.png\"")));
	}

	@DisplayName("Inline query has phrase that could not be spelled")
	@Test
	public void inlineAnswerIfAnySpellerCouldNotSpellPhrase() {
		// given
		JSONObject from = new JSONObject(), message = new JSONObject();

		from.put("language_code", "en");
		message.put("from", from);
		message.put("id", 4321);
		message.put("query", "top");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processInlineQuery(message);

		// then
		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"inline_query_id\":4321")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"message_text\":\"wrong letters\"")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"title\":\"argh\"")),
				() -> assertThat(responseEvent.get().getBody(), containsString("\"type\":\"article\"")),
				() -> assertThat(responseEvent.get().getBody(),
						containsString("\"thumb_url\":\"http://example.com/icon/error.png\"")));
	}

	@BeforeEach
	public void setUp() {
		lambdaHandler = new TelegramHandler();
	}

}