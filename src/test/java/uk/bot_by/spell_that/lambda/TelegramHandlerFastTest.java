package uk.bot_by.spell_that.lambda;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.LoggerFactory;
import uk.bot_by.spell_that.speller.SpellThatService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static co.unruly.matchers.OptionalMatchers.empty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Tag("fast")
class TelegramHandlerFastTest {

	@Mock
	private APIGatewayProxyResponseEvent responseEvent;
	@Mock
	private Appender<ILoggingEvent> appender;
	@Captor
	private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
	@Mock
	private Context context;
	@Mock
	private SpellThatService spellThatService;

	@Spy
	private TelegramHandler lambdaHandler;

	private APIGatewayProxyRequestEvent requestEvent;

	@DisplayName("Handle a wrong request, write IP address to info log.")
	@Test
	public void handleWrongRequest() {
		// given
		requestEvent.setBody("name1=value1&name2=value2");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler, never()).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler, never()).processMessage(isA(JSONObject.class));
		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a warning message",
				() -> assertEquals(
						"Wrong request from 1.2.3.4: " +
								"A JSONObject text must begin with '{' at 1 [character 2 line 1]\n" +
								"name1=value1&name2=value2",
						loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.WARN, loggingEvents.get(0).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("OK", responseEvent.getBody(), "Response body"),
				() -> assertEquals(200, responseEvent.getStatusCode(), "Response status"));
	}

	@DisplayName("Handle an empty request, write IP address to info log.")
	@ParameterizedTest(name = "body <{argumentsWithNames}>")
	@NullAndEmptySource
	@ValueSource(strings = "  ")
	public void handleEmptyRequest(String body) {
		// given
		requestEvent.setBody(body);

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler, never()).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler, never()).processMessage(isA(JSONObject.class));
		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log an info message",
				() -> assertEquals("Empty request from 1.2.3.4", loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.INFO, loggingEvents.get(0).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("OK", responseEvent.getBody(), "Response body"),
				() -> assertEquals(200, responseEvent.getStatusCode(), "Response status"));
	}

	@DisplayName("Process an inline query")
	@Test
	public void processInlineQuery() {
		// given
		requestEvent.setBody("{ \"inline_query\": { \"query\": \"qwerty\" }}");
		doReturn(Optional.of(responseEvent)).when(lambdaHandler).processInlineQuery(isA(JSONObject.class));

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler, never()).processMessage(isA(JSONObject.class));
		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a trace message",
				() -> assertEquals("Request body:\n{\"inline_query\":{\"query\":\"qwerty\"}}",
						loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("test response", responseEvent.getBody(), "Response body"));
	}

	@DisplayName("Process a message")
	@Test
	public void processMessage() {
		// given
		requestEvent.setBody("{ \"message\": { \"text\": \"test text\" }}");
		doReturn(Optional.of(responseEvent)).when(lambdaHandler).processMessage(isA(JSONObject.class));

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler, never()).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler).processMessage(isA(JSONObject.class));
		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a trace message",
				() -> assertEquals("Request body:\n{\"message\":{\"text\":\"test text\"}}",
						loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("test response", responseEvent.getBody(), "Response body"));
	}

	@DisplayName("Non-text message is ignored")
	@Test
	public void nonTextMessage() {
		// given
		requestEvent.setBody("{ \"message\": { \"new_chat_title\": \"test title\" }}");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler, never()).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler, never()).processMessage(isA(JSONObject.class));
		verify(appender, atLeast(2)).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a trace message",
				() -> assertEquals("Request body:\n{\"message\":{\"new_chat_title\":\"test title\"}}",
						loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel()));
		assertAll("Write to the log an info \"unprocessed\" message",
				() -> assertEquals("Unprocessed update: [message]",
						loggingEvents.get(1).getFormattedMessage()),
				() -> assertEquals(Level.INFO, loggingEvents.get(1).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("OK", responseEvent.getBody(), "Response body"),
				() -> assertEquals(200, responseEvent.getStatusCode(), "Response status"));
	}

	@DisplayName("Unprocessed update")
	@Test
	public void unprocessedUpdate() {
		// given
		requestEvent.setBody("{ \"chosen_inline_result\": { query: qwerty }}");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler, never()).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler, never()).processMessage(isA(JSONObject.class));
		verify(appender, atLeast(2)).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a trace message",
				() -> assertEquals("Request body:\n{\"chosen_inline_result\":{\"query\":\"qwerty\"}}",
						loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel()));
		assertAll("Write to the log an info \"unprocessed\" message",
				() -> assertEquals("Unprocessed update: [chosen_inline_result]",
						loggingEvents.get(1).getFormattedMessage()),
				() -> assertEquals(Level.INFO, loggingEvents.get(1).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("OK", responseEvent.getBody(), "Response body"),
				() -> assertEquals(200, responseEvent.getStatusCode(), "Response status"));
	}

	@DisplayName("Start and help commands with parameters are ignored")
	@ParameterizedTest(name = "Command {argumentsWithNames}")
	@ValueSource(strings = {"start", "help"})
	public void commandWithParameters(String commandName) {
		// given
		String command = "/" + commandName;
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), startCommand = new JSONObject();

		chat.put("id", 4321L);
		startCommand.put("length", command.length());
		startCommand.put("offset", 0);
		startCommand.put("type", "bot_command");
		entities.put(startCommand);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", command + " abc");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertThat("Response empty", responseEvent, empty());
	}

	@DisplayName("Ignore commands")
	@Test
	public void twoCommands() {
		// given
		JSONArray entities = new JSONArray();
		JSONObject buyCommand = new JSONObject(), chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(),
				startCommand = new JSONObject();

		chat.put("id", 4321L);
		buyCommand.put("length", "/buy".length());
		buyCommand.put("offset", "/start /buy".indexOf("/buy"));
		buyCommand.put("type", "bot_command");
		startCommand.put("length", "/start".length());
		startCommand.put("offset", 0);
		startCommand.put("type", "bot_command");
		entities.put(buyCommand);
		entities.put(startCommand);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", "/start /buy");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertThat("Response empty", responseEvent, empty());
	}

	@DisplayName("Ignore command that do not start message")
	@Test
	public void commandDoNotStartMessage() {
		// given
		String command = "/start";
		String messageWithCommand = "qwerty " + command;
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), startCommand = new JSONObject();

		chat.put("id", 4321L);
		startCommand.put("length", command.length());
		startCommand.put("offset", messageWithCommand.indexOf(command));
		startCommand.put("type", "bot_command");
		entities.put(startCommand);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", messageWithCommand);

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertThat("Response empty", responseEvent, empty());
	}

	@DisplayName("Ignore an unknown command")
	@Test
	public void unknownCommand() {
		// given
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), startCommand = new JSONObject();

		chat.put("id", 4321L);
		startCommand.put("length", "/buy".length());
		startCommand.put("offset", 0);
		startCommand.put("type", "bot_command");
		entities.put(startCommand);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", "/buy");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertThat("Response empty", responseEvent, empty());
	}

	@DisplayName("Ignore messages are sent via a bot")
	@Test
	public void viaBot() {
		// given
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), viaBot = new JSONObject();

		chat.put("id", 4321L);
		from.put("language_code", "en");
		viaBot.put("id", 567L);
		message.put("chat", chat);
		message.put("from", from);
		message.put("via_bot", viaBot);
		message.put("text", "/start");

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertThat("Response empty", responseEvent, empty());
	}

	@DisplayName("Ignore non-text messages")
	@ParameterizedTest(name = "Text is <{arguments}>")
	@NullAndEmptySource
	@ValueSource(strings = " ")
	public void nonTextMessage(String text) {
		// given
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject();

		chat.put("id", 4321L);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("from", from);
		if (null != text) {
			message.put("text", text);
		}

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		assertThat("Response empty", responseEvent, empty());
	}

	@DisplayName("Spell simple text message")
	@Test
	public void simpleTextMessage() {
		// given
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject();

		chat.put("id", 4321L);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("from", from);
		message.put("text", "test message");

		when(spellThatService.spell(anyString())).thenReturn(Arrays.asList("qwerty", "abc", "xyz"));
		doReturn(spellThatService).when(lambdaHandler).getSpellThatService();

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(spellThatService).spell(eq("test message"));

		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(),
						containsString("\"text\":\"qwerty abc xyz\"")));
	}

	@DisplayName("Speller could not spell simple text message")
	@Test
	public void simpleTextMessageIsNotSpelled() {
		// given
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject();

		chat.put("id", 4321L);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("from", from);
		message.put("text", "test message");

		when(spellThatService.spell(anyString())).thenReturn(Collections.emptyList());
		doReturn(spellThatService).when(lambdaHandler).getSpellThatService();

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(spellThatService).spell(eq("test message"));

		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(),
						containsString("\"text\":\"wrong letters\"")));
	}

	@DisplayName("Spell text message with entities")
	@Test
	public void complexTextMessage() {
		// given
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), cashtag = new JSONObject();

		chat.put("id", 4321L);
		cashtag.put("length", "$USD".length());
		cashtag.put("offset", "test $USD message".indexOf("$USD"));
		cashtag.put("type", "cashtag");
		entities.put(cashtag);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", "test $USD message");

		when(spellThatService.spell(anyString())).thenReturn(Arrays.asList("qwerty", "abc", "xyz"));
		doReturn(spellThatService).when(lambdaHandler).getSpellThatService();

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(spellThatService).spell(eq("test message"));

		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(),
						containsString("\"text\":\"qwerty abc xyz\"")));
	}

	@DisplayName("Speller could not spell simple text message")
	@Test
	public void complexTextMessageIsNotSpelled() {
		// given
		JSONArray entities = new JSONArray();
		JSONObject chat = new JSONObject(), from = new JSONObject(), message = new JSONObject(), cashtag = new JSONObject();

		chat.put("id", 4321L);
		cashtag.put("length", "$USD".length());
		cashtag.put("offset", "test $USD message".indexOf("$USD"));
		cashtag.put("type", "cashtag");
		entities.put(cashtag);
		from.put("language_code", "en");
		message.put("chat", chat);
		message.put("entities", entities);
		message.put("from", from);
		message.put("text", "test $USD message");

		when(spellThatService.spell(anyString())).thenReturn(Collections.emptyList());
		doReturn(spellThatService).when(lambdaHandler).getSpellThatService();

		// when
		Optional<APIGatewayProxyResponseEvent> responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(spellThatService).spell(eq("test message"));

		assertAll("Spelling response",
				() -> assertThat("Response contains event", responseEvent, not(empty())),
				() -> assertThat(responseEvent.get().getBody(),
						containsString("\"text\":\"wrong letters\"")));
	}

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);

		requestEvent = new APIGatewayProxyRequestEvent();
		requestEvent.setHttpMethod("POST");
		requestEvent.setHeaders(Collections.singletonMap("x-forwarded-for", "1.2.3.4"));

		((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);

		when(context.getAwsRequestId()).thenReturn("AWS ReqId");
		when(responseEvent.getBody()).thenReturn("test response");
	}

}