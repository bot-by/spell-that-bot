package uk.bot_by.spell_that.speller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static co.unruly.matchers.StreamMatchers.contains;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

@Tag("fast")
class OnePassSpellerTest {

	private static Map<Character, String> translationMap;

	private OnePassSpeller speller;

	static List<Map<Integer, String>> wrongTranslationMap() {
		return Arrays.asList(Collections.emptyMap(), Collections.singletonMap(null, "qwerty"));
	}

	@DisplayName("Empty map and map with null key")
	@ParameterizedTest
	@MethodSource
	public void wrongTranslationMap(Map<Character, String> wrongTranslationMap) {
		// when
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new OnePassSpeller("one_pass_speller", "one pass speller", wrongTranslationMap),
				"Translation map could be not empty or null and has at least one non-null key");

		// then
		assertEquals("Require at least one translation mapping.", exception.getMessage(), "Exception message");
	}

	@DisplayName("Null map")
	@ParameterizedTest
	@NullSource
	public void nullTranslationMap(Map<Character, String> wrongTranslationMap) {
		// when
		NullPointerException exception = assertThrows(NullPointerException.class,
				() -> new OnePassSpeller("one_pass_speller", "one pass speller", wrongTranslationMap),
				"Translation map could be not empty or null and has at least one non-null key");

		// then
		assertEquals("Translation map cannot be null.", exception.getMessage(), "Exception message");
	}

	@DisplayName("Invalid character")
	@Test
	public void invalidCharacter() {
		// given
		char testCharacter = 'B';

		// when
		boolean characterIsValid = speller.isValidCharacters(Stream.of(testCharacter));

		// then
		assertFalse(characterIsValid, "Character is invalid");
	}

	@DisplayName("Valid character")
	@ParameterizedTest
	@ValueSource(chars = {'a', 'A'})
	public void validCharacter(char testCharacter) {
		// when
		boolean characterIsValid = speller.isValidCharacters(Stream.of(testCharacter));

		// then
		assertTrue(characterIsValid, "Character is valid");
	}

	@DisplayName("Happy path")
	@Test
	public void happyPath() {
		// given
		String testWord = "Apple";

		assumeTrue(speller.isValidCharacters(testWord.chars().mapToObj(character -> (char) character)),
				"Translation map contains all characters");

		// when
		Stream<String> actualSpelling = speller.spell(testWord);

		// then
		assertThat("Spelling is correct", actualSpelling, contains("Alpha", "Papa", "Papa", "Lima", "Echo"));
	}

	@DisplayName("Speller could skip some characters")
	@Test
	public void skipDash() {
		// given
		String testWord = "ap-pLe*";

		assumeTrue(speller.isValidCharacters(testWord.chars().mapToObj(character -> (char) character)),
				"Translation map contains all characters");

		// when
		Stream<String> actualSpelling = speller.spell(testWord);

		// then
		assertThat("Dash and star are skipped", actualSpelling, contains("Alpha", "Papa", "Papa", "Lima", "Echo"));
	}

	@DisplayName("Translation map is unmodifiable")
	@Test
	public void translationMapIsUnmodifiable() {
		// when
		assertThrows(UnsupportedOperationException.class, () -> speller.getTranslationMap().put('h', "Hotel"),
				"Translation map is unmodifiable");
	}

	@BeforeAll
	public static void setUpClass() {
		translationMap = new HashMap<>();

		translationMap.put('a', "Alpha");
		translationMap.put('E', "Echo");
		translationMap.put('l', "Lima");
		translationMap.put('P', "Papa");
		translationMap.put('-', "");
		translationMap.put('*', "");
	}

	@BeforeEach
	public void setUp() {
		speller = new OnePassSpeller("one_pass_speller", "one pass speller", translationMap);
	}

}