package uk.bot_by.spell_that.speller;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Tag("fast")
class SpellThatServiceFastTest {

	@Mock
	private Appender<ILoggingEvent> appender;
	@Captor
	private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

	@DisplayName("Create a service with speller array: speller array is empty or blank")
	@Test
	public void emptySpellerArray() {
		// when
		Exception exception = assertThrows(IllegalArgumentException.class, SpellThatService::new,
				"Could not create a service if it has no spellers");

		// then
		assertEquals("Require at least one speller instance.", exception.getMessage());
	}

	@DisplayName("Create a service with speller array: it creates a service with three spellers")
	@Test
	public void createServiceWithSpellers() {
		// given
		Speller spellerA = mock(Speller.class), spellerB = mock(Speller.class), spellerC = mock(Speller.class);

		when(spellerA.getName()).thenReturn("A");
		when(spellerB.getName()).thenReturn("B");
		when(spellerC.getName()).thenReturn("C");

		// when
		SpellThatService service = assertDoesNotThrow(() -> new SpellThatService(spellerB, spellerA, spellerC),
				"Create a service with some speller");

		// then
		assertAll("A service with three spellers",
				() -> assertThat("Three spellers", service.getSpellerNames(), hasSize(3)),
				() -> assertThat("Speller names", service.getSpellerNames(), containsInAnyOrder("C", "B", "A")));
	}

	@DisplayName("Create a service with speller array: it ignores null spellers")
	@Test
	public void nullSpeller() {
		// given
		Speller spellerA = mock(Speller.class), spellerC = mock(Speller.class);

		when(spellerA.getName()).thenReturn("A");
		when(spellerC.getName()).thenReturn("C");

		// when
		SpellThatService service = assertDoesNotThrow(() -> new SpellThatService(spellerA, null, spellerC),
				"Create a service with at least one null speller");

		// then
		assertAll("A service with three spellers",
				() -> assertThat("Two spellers", service.getSpellerNames(), hasSize(2)),
				() -> assertThat("Speller names", service.getSpellerNames(), containsInAnyOrder("C", "A")));
	}

	@DisplayName("First speller is used by default")
	@Test
	public void firstSpellerIsDefaultOne() {
		// given
		Speller spellerA = mock(Speller.class), spellerB = mock(Speller.class), spellerC = mock(Speller.class);

		when(spellerA.getName()).thenReturn("A");
		when(spellerB.getName()).thenReturn("B");
		when(spellerB.spell(anyString())).thenReturn(Stream.of("Test", "passed"));
		when(spellerC.getName()).thenReturn("C");

		SpellThatService service = new SpellThatService(spellerB, spellerA, spellerC);

		// when
		List<String> actualSpelling = service.spell("qwerty");

		// then
		verify(spellerA, never()).spell(anyString());
		verify(spellerB).spell(eq("qwerty"));
		verify(spellerC, never()).spell(anyString());

		assertThat("Return test spelling", actualSpelling, contains("Test", "passed"));
	}

	@DisplayName("If a speller name is unknown it uses a default speller")
	@Test
	public void unknownSpellerName() {
		// given
		Speller spellerA = mock(Speller.class), spellerC = mock(Speller.class);

		when(spellerA.getName()).thenReturn("A");
		when(spellerA.spell(anyString())).thenReturn(Stream.of("Test", "passed"));
		when(spellerC.getName()).thenReturn("C");

		SpellThatService service = new SpellThatService(spellerA, spellerC);

		// when
		List<String> actualSpelling = service.spell("qwerty", "B");

		// then
		verify(spellerA).spell(eq("qwerty"));
		verify(spellerC, never()).spell(anyString());

		assertThat("Return test spelling", actualSpelling, contains("Test", "passed"));

		verify(appender, times(2)).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a debug message",
				() -> assertThat(loggingEvents.get(0).getFormattedMessage(), startsWith("Spellers: [")),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel()),
				() -> assertEquals("Requested speller B is not found, use A instead", loggingEvents.get(1).getFormattedMessage()),
				() -> assertEquals(Level.DEBUG, loggingEvents.get(1).getLevel()));
	}

	@DisplayName("Happy path: find a speller by name, spell a string")
	@Test
	public void happyPath() {
		// given
		Speller speller = mock(Speller.class);

		when(speller.getName()).thenReturn("speller");
		when(speller.spell(anyString())).thenReturn(Stream.of("Test", "passed"));

		SpellThatService service = new SpellThatService(speller);

		// when
		List<String> actualSpelling = service.spell("qwerty", "speller");

		// then
		verify(speller).spell(eq("qwerty"));

		assertThat("Return test spelling", actualSpelling, contains("Test", "passed"));

		verify(appender).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a debug message",
				() -> assertEquals("Spellers: [speller*]", loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel()));
	}

	@DisplayName("Speller couldn't spell that")
	@Test
	public void misspelledPhrase() {
		// given
		Speller speller = mock(Speller.class);

		when(speller.getName()).thenReturn("speller");
		when(speller.spell(anyString())).thenReturn(Stream.empty());

		SpellThatService service = new SpellThatService(speller);

		// when
		List<String> actualSpelling = service.spell("qwerty", "speller");

		// then
		verify(speller).spell(eq("qwerty"));

		assertThat("Return empty spelling", actualSpelling, empty());

		verify(appender).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a debug message",
				() -> assertEquals("Spellers: [speller*]", loggingEvents.get(0).getFormattedMessage(), "Speller names"),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel(), "Speller names"));
	}

	@DisplayName("It throws exception if that is null")
	@Test
	public void nullThat() {
		// given
		Speller speller = mock(Speller.class);

		when(speller.getName()).thenReturn("speller");

		SpellThatService service = new SpellThatService(speller);

		// when
		NullPointerException exception = assertThrows(NullPointerException.class, () -> service.spell(null, "speller"));

		// then
		verify(speller, never()).spell(anyString());

		assertEquals("That cannot be null.", exception.getMessage(), "Exception message");
	}

	@DisplayName("toString()")
	@Test
	public void convertToString() {
		// given
		Speller speller = mock(Speller.class);

		when(speller.getName()).thenReturn("speller");

		SpellThatService service = new SpellThatService(speller);

		// when
		String actualString = service.toString();

		// then
		assertEquals("SpellThatService{spellers = [speller*]}", actualString, "String with speller set");
	}

	@DisplayName("Create a service with speller names: speller name list is null")
	@Test
	public void createServiceIfSpellerNameListIsNull() {
		// given
		String spellerNamesIsNull = null;

		// when
		Exception exception = assertThrows(NullPointerException.class, () -> new SpellThatService(spellerNamesIsNull));

		// then
		assertEquals("Spellers do not defined", exception.getMessage());
	}

	@DisplayName("Get spellers: speller name list is null")
	@Test
	public void spellerNameListIsNull() {
		// when
		Exception exception = assertThrows(NullPointerException.class, () -> SpellThatService.getSpellers(null));

		// then
		assertEquals("Spellers do not defined", exception.getMessage());
	}

	@DisplayName("Get spellers: happy path, get spellers from property files")
	@Test
	public void getSpellerFromResources() {
		// when
		Speller[] spellers = SpellThatService.getSpellers("test1, Test2,TEST 3, test4 ,i-am-not-exist");

		// then
		assertAll("Get spellers from resources",
				() -> assertEquals(4, spellers.length),
				() -> assertThat(Arrays.stream(spellers).map(Speller::getName).collect(Collectors.toList()),
						contains("test1", "Test2", "TEST 3", "test4")));
	}

	@DisplayName("Get speller id from its name")
	@ParameterizedTest
	@CsvSource({"BBC, bbc", "CamelCase, camel_case", "With  Spaces, with_spaces"})
	public void spellerId(String spellerName, String spellerId) {
		// when
		String actualSpellerId = SpellThatService.getSpellerId(spellerName);

		// then
		assertEquals(spellerId, actualSpellerId);
	}

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);

		Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

		logger.addAppender(appender);
	}

}