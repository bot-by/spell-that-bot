package uk.bot_by.spell_that.speller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;

import java.util.stream.Stream;

import static co.unruly.matchers.StreamMatchers.contains;
import static co.unruly.matchers.StreamMatchers.empty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@Tag("fast")
class AbstractCharacterSpellerTest {

	@Captor
	private ArgumentCaptor<Stream<Character>> captorStream;

	private AbstractCharacterSpeller speller;

	@DisplayName("Spelling characters")
	@Test
	public void characterSpelling() {
		// when
		Stream<String> actualSpelling = speller.spell("Abc");

		// then
		verify(speller).isValidCharacters(captorStream.capture());

		assertThat("Validation stream", captorStream.getValue(), contains('A', 'b', 'c'));

		verify(speller).spellCharacters(captorStream.capture());

		assertThat("Spelling stream", captorStream.getValue(), contains('A', 'b', 'c'));

		assertThat("Result of spelling", actualSpelling, contains("Test", "passed"));
	}

	@DisplayName("Could not spell characters")
	@Test
	public void couldNotSpellCharacters() {
		// given
		doReturn(false).when(speller).isValidCharacters(any());

		// when
		Stream<String> actualSpelling = speller.spell("Abc");

		// then
		verify(speller).isValidCharacters(captorStream.capture());

		assertThat("Validation stream", captorStream.getValue(), contains('A', 'b', 'c'));

		verify(speller, never()).spellCharacters(any());

		assertThat("Empty result of spelling", actualSpelling, empty());
	}

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.openMocks(this);

		speller = spy(new TestCharacterSpeller("character speller"));
	}

	private static class TestCharacterSpeller extends AbstractCharacterSpeller {

		protected TestCharacterSpeller(String name) {
			super("id", name);
		}

		@Override
		public boolean isValidCharacters(Stream<Character> characters) {
			return true;
		}

		@Override
		public Stream<String> spellCharacters(Stream<Character> characters) {
			return Stream.of("Test", "passed");
		}
	}

}