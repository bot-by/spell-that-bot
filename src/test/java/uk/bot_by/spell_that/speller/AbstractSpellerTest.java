package uk.bot_by.spell_that.speller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Tag("fast")
class AbstractSpellerTest {

	@DisplayName("Check speller name")
	@ParameterizedTest
	@ValueSource(strings = {"qwerty", "abc def", " wxyz  "})
	public void id(String id) {
		// when
		TestSpeller speller = assertDoesNotThrow(() -> new TestSpeller(id, "spellerName"), "Speller has id");

		// then
		assertEquals(id.trim(), speller.getId(), "Speller id");
	}

	@DisplayName("Check speller name")
	@ParameterizedTest
	@ValueSource(strings = {"qwerty", "abc def", " wxyz  "})
	public void name(String spellerName) {
		// when
		TestSpeller speller = assertDoesNotThrow(() -> new TestSpeller("id", spellerName), "Speller is named");

		// then
		assertEquals(spellerName.trim(), speller.getName(), "Speller name");
	}

	@DisplayName("Speller id is not null")
	@ParameterizedTest
	@NullSource
	public void nullId(String id) {
		// when
		NullPointerException exception = assertThrows(NullPointerException.class, () -> new TestSpeller(id, "spellerName"));

		// then
		assertEquals("Id cannot be null.", exception.getMessage(), "Exception message");
	}

	@DisplayName("Speller name is not null")
	@ParameterizedTest
	@NullSource
	public void nullName(String spellerName) {
		// when
		NullPointerException exception = assertThrows(NullPointerException.class, () -> new TestSpeller("id", spellerName));

		// then
		assertEquals("Name cannot be null.", exception.getMessage(), "Exception message");
	}

	@DisplayName("Speller id is not empty")
	@ParameterizedTest
	@ValueSource(strings = "   ")
	@EmptySource
	public void emptyId(String id) {
		// when
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new TestSpeller(id, "spellerName"));

		// then
		assertEquals("Id cannot be empty or blank", exception.getMessage(), "Exception message");
	}

	@DisplayName("Speller name is not empty")
	@ParameterizedTest
	@ValueSource(strings = "   ")
	@EmptySource
	public void emptyName(String spellerName) {
		// when
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new TestSpeller("id", spellerName));

		// then
		assertEquals("Name cannot be empty or blank", exception.getMessage(), "Exception message");
	}

	private static class TestSpeller extends AbstractSpeller {

		protected TestSpeller(String id, String name) {
			super(id, name);
		}

		@Override
		public Stream<String> spell(String that) {
			return Stream.empty();
		}
	}

}