package uk.bot_by.spell_that.speller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Tag("slow")
public class SpellThatServiceSlowTest {

	@DisplayName("Create the speller service with English and Phone number spellers")
	@Test
	public void createInstanceWithSpellers() {
		// when and then
		assertNotNull(
				assertDoesNotThrow(() -> new SpellThatService("English,Phone number"),
						"The constructor does not throw any exceptions"), "Returns a speller service");
	}

}
