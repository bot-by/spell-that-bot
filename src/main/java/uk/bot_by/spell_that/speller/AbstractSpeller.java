package uk.bot_by.spell_that.speller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Base named speller class.
 */
public abstract class AbstractSpeller implements Speller {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	private final String id;
	private final String name;

	/**
	 * Creates a named speller.
	 *
	 * @param name speller name; required
	 * @throws NullPointerException     if name is null
	 * @throws IllegalArgumentException if name is empty or blank
	 */
	protected AbstractSpeller(String id, String name) {
		Objects.requireNonNull(id, "Id cannot be null.");
		if (id.isBlank()) {
			throw new IllegalArgumentException("Id cannot be empty or blank");
		}
		Objects.requireNonNull(name, "Name cannot be null.");
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name cannot be empty or blank");
		}

		this.id = id.strip();
		this.name = name.strip();
		logger.trace("Create speller {}:{}", getId(), getName());
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

}
