package uk.bot_by.spell_that.speller;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * One pass speller.
 */
public class OnePassSpeller extends AbstractCharacterSpeller {

	private final Map<Character, String> translationMap;

	public OnePassSpeller(String id, String name, Map<Character, String> translationMap) {
		super(id, name);
		Objects.requireNonNull(translationMap, "Translation map cannot be null.");
		translationMap.keySet().stream()
				.filter(Objects::nonNull)
				.findFirst()
				.orElseThrow(() -> new IllegalArgumentException("Require at least one translation mapping."));

		this.translationMap = Collections.unmodifiableMap(translationMap.entrySet().stream()
				.collect(Collectors.toMap(entry -> Character.toLowerCase(entry.getKey()), Map.Entry::getValue)));
	}

	@Override
	protected boolean isValidCharacters(Stream<Character> characters) {
		return characters.map(Character::toLowerCase).allMatch(getTranslationMap()::containsKey);
	}

	@Override
	protected Stream<String> spellCharacters(Stream<Character> characters) {
		return characters.map(Character::toLowerCase).map(getTranslationMap()::get).filter(Predicate.not(String::isBlank));
	}

	protected Map<Character, String> getTranslationMap() {
		return translationMap;
	}

}
