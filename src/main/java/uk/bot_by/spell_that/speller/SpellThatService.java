package uk.bot_by.spell_that.speller;

import org.jetbrains.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Spell That Service.
 */
public class SpellThatService {

	private static final String LEADING_GAP = "_$1";
	private static final Logger LOGGER = LoggerFactory.getLogger(SpellThatService.class);
	private static final Pattern PATTERN_CAMEL_CASE =
			Pattern.compile("(?<=[\\p{L}&&[^\\p{Lu}]])(\\p{Lu})(?=[\\p{L}&&[^\\p{Lu}]])");
	private static final Pattern PATTERN_SPACE = Pattern.compile("\\s+");
	private static final String REGEX_COMMA = ",+";
	private static final String REGEX_POINT = "\\.";
	private static final String SLASH = "/";
	private static final String UNDERSCORE = "_";

	private final Speller defaultSpeller;
	private final Map<String, Speller> spellerMap;

	/**
	 * Create Spell That Service with spellers that loaded from resources by their names.
	 * <p>
	 * Require comma separated list of speller resource names.
	 * <p>
	 * Before loading resource it make name lowercase, trim leading spaces,
	 * replace other spaces with underscore.
	 *
	 * @param spellerNames comma separated list of speller resource names
	 * @throws NullPointerException if spellerNames is null
	 */
	public SpellThatService(String spellerNames) {
		this(getSpellers(spellerNames));
	}

	/**
	 * Create Spell That Service with spellers.
	 * <p>
	 * Require at least one speller. First speller is used as default. Nulls are ignored.
	 *
	 * @param spellers array (varargs) of spellers; first (not null) is used as default
	 * @throws IllegalArgumentException if speller array has size 0 or have nulls only
	 */
	public SpellThatService(Speller... spellers) {
		defaultSpeller = Stream.of(spellers)
				.filter(Objects::nonNull)
				.findFirst()
				.orElseThrow(() -> new IllegalArgumentException("Require at least one speller instance."));
		spellerMap = Stream.of(spellers)
				.filter(Objects::nonNull)
				.collect(Collectors.toMap(Speller::getName, Function.identity()));
		LOGGER.trace("Spellers: {}", getSpellerNamesWithStarredDefault());
	}

	public static String getSpellerId(String spellerName) {
		String spellerId;

		spellerId = PATTERN_CAMEL_CASE.matcher(spellerName).replaceAll(LEADING_GAP);
		spellerId = PATTERN_SPACE.matcher(spellerId).replaceAll(UNDERSCORE);
		spellerId = spellerId.toLowerCase();
		LOGGER.trace("Speller Id: {} => {}", spellerName, spellerId);

		return spellerId;
	}

	@VisibleForTesting
	static Speller[] getSpellers(String spellerNameList) {
		Objects.requireNonNull(spellerNameList, "Spellers do not defined");

		String prefix = SpellThatService.class.getPackageName().replaceAll(REGEX_POINT, SLASH) + SLASH;
		String[] spellerNames = spellerNameList.split(REGEX_COMMA);
		List<Speller> spellers = new ArrayList<>(spellerNames.length);
		ResourceBundle resourceBundle;
		Map<Character, String> spellingMap;
		String spellerId;

		for (String spellerName : spellerNames) {
			spellerName = spellerName.strip();
			try {
				spellerId = getSpellerId(spellerName);
				resourceBundle = ResourceBundle.getBundle(prefix + spellerId);
				spellingMap = new HashMap<>();
				for (String key : resourceBundle.keySet()) {
					spellingMap.put(key.charAt(0), resourceBundle.getString(key));
				}
				spellers.add(new OnePassSpeller(spellerId, spellerName, spellingMap));
			} catch (MissingResourceException missingSpellerResource) {
				LOGGER.warn("Speller <{}> is missed", spellerName);
			}
		}

		return spellers.toArray(Speller[]::new);
	}

	/**
	 * Return speller names.
	 *
	 * @return set of speller names
	 */
	public Set<String> getSpellerNames() {
		return spellerMap.keySet();
	}

	/**
	 * Spell that with default speller.
	 *
	 * @param that spelled word
	 * @return spelling as list of strings
	 */
	public List<String> spell(String that) {
		return spell(that, null);
	}

	/**
	 * Spell that with particular speller. If a speller is not found it takes default one.
	 *
	 * @param that        spelled word
	 * @param spellerName speller name.
	 * @return spelling as list of strings
	 */
	public List<String> spell(String that, String spellerName) {
		Objects.requireNonNull(that, "That cannot be null.");

		Speller speller = spellerMap.getOrDefault(spellerName, defaultSpeller);

		if (null != spellerName && !speller.getName().equals(spellerName)) {
			LOGGER.debug("Requested speller {} is not found, use {} instead", spellerName, speller.getName());
		}

		return speller.spell(that).collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{spellers = " + getSpellerNamesWithStarredDefault() + "}";
	}

	private Set<String> getSpellerNamesWithStarredDefault() {
		return getSpellerNames().stream()
				.map(spellerName -> (spellerName.equals(defaultSpeller.getName())) ? spellerName + '*' : spellerName)
				.collect(Collectors.toSet());
	}

}
