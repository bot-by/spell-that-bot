package uk.bot_by.spell_that.speller;

import java.util.stream.Stream;

public interface Speller {

	String getId();

	String getName();

	Stream<String> spell(String that);

}
