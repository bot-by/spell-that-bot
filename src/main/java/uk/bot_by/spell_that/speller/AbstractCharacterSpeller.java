package uk.bot_by.spell_that.speller;

import java.util.function.IntFunction;
import java.util.stream.Stream;

/**
 * Base character speller class.
 */
public abstract class AbstractCharacterSpeller extends AbstractSpeller {

	protected static final IntFunction<Character> INT_TO_CHARACTER = (character) -> (char) character;

	protected AbstractCharacterSpeller(String id, String name) {
		super(id, name);
	}

	protected abstract boolean isValidCharacters(Stream<Character> characters);

	protected abstract Stream<String> spellCharacters(Stream<Character> characters);

	@Override
	public Stream<String> spell(String that) {
		Stream<String> spelling = Stream.empty();

		if (isValidCharacters(that.chars().mapToObj(INT_TO_CHARACTER))) {
			logger.trace("Spell \"{}\" with {}", that, getName());
			spelling = spellCharacters(that.chars().mapToObj(INT_TO_CHARACTER));
		} else {
			logger.debug("Couldn't spell \"{}\" with {}", that, getName());
		}

		return spelling;
	}

}
