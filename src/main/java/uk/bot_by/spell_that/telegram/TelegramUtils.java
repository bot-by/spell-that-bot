package uk.bot_by.spell_that.telegram;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.bot_by.spell_that.lambda.TelegramHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static uk.bot_by.spell_that.telegram.TelegramField.CacheTime;
import static uk.bot_by.spell_that.telegram.TelegramField.ChatID;
import static uk.bot_by.spell_that.telegram.TelegramField.Description;
import static uk.bot_by.spell_that.telegram.TelegramField.DisableWebPagePreview;
import static uk.bot_by.spell_that.telegram.TelegramField.Id;
import static uk.bot_by.spell_that.telegram.TelegramField.InlineQueryId;
import static uk.bot_by.spell_that.telegram.TelegramField.InputMessageContent;
import static uk.bot_by.spell_that.telegram.TelegramField.IsPersonal;
import static uk.bot_by.spell_that.telegram.TelegramField.MessageText;
import static uk.bot_by.spell_that.telegram.TelegramField.Method;
import static uk.bot_by.spell_that.telegram.TelegramField.ParseMode;
import static uk.bot_by.spell_that.telegram.TelegramField.Results;
import static uk.bot_by.spell_that.telegram.TelegramField.Text;
import static uk.bot_by.spell_that.telegram.TelegramField.ThumbHeight;
import static uk.bot_by.spell_that.telegram.TelegramField.ThumbLocation;
import static uk.bot_by.spell_that.telegram.TelegramField.ThumbWidth;
import static uk.bot_by.spell_that.telegram.TelegramField.Title;
import static uk.bot_by.spell_that.telegram.TelegramField.Type;

public class TelegramUtils {

	private static final String ANSWER_INLINE_QUERY = "answerInlineQuery";
	private static final String BLANK = "";
	private static final String CHAT_ICON;
	private static final String EMPTY = "empty";
	private static final Function<JSONArray, Stream<JSONObject>> ENTITY_STREAM =
			(entities) -> StreamSupport.stream(entities.spliterator(), true)
					.map(object -> (JSONObject) object);
	private static final String ERROR_ICON;
	private static final int ICON_SIZE = 256;
	private static final List<String> IGNORED_ENTITIES =
			Arrays.asList("mention", "hashtag", "cashtag", "bot_command", "url", "text_link", "text_mention");
	private static final String INFO_ICON;
	private static final String INLINE_QUERY_RESULT_ARTICLE = "article";
	private static final String LENGTH = "length";
	private static final Logger LOGGER = LoggerFactory.getLogger(TelegramHandler.class);
	private static final int MAX_ID_LENGTH = 64;
	private static final String OFFSET = "offset";
	private static final int ONE_WEEK = 604800;
	private static final String PARSE_MODE_MARKDOWN_V2 = "MarkdownV2";
	private static final String SEND_MESSAGE = "sendMessage";
	private static final String ICON_PATH = "ICON_PATH";
	private static final String REGEX_SPACE = "\\s";
	private static final String SPACE = " ";
	private static final String TYPE = "type";
	private static final Boolean WEB_PAGE_PREVIEW_DISABLED = true;
	private static final Boolean WEB_PAGE_PREVIEW_ENABLED = false;
	private static final int ZERO = 0;

	static {
		String iconPathFormat = System.getenv(ICON_PATH);

		Objects.requireNonNull(iconPathFormat, "Icon path is missed");

		CHAT_ICON = String.format(iconPathFormat, "chat");
		ERROR_ICON = String.format(iconPathFormat, "error");
		INFO_ICON = String.format(iconPathFormat, "info");
	}

	public static String clearSpelledText(String text, JSONArray entities) {
		StringBuilder textWithEntities = new StringBuilder(text);

		LOGGER.trace("Text with entities: {}", text);
		ENTITY_STREAM.apply(entities)
				.filter(entity -> IGNORED_ENTITIES.contains(entity.getString(TYPE)))
				.forEach(entity -> {
					int offset = entity.getInt(OFFSET);
					int length = entity.getInt(LENGTH);
					textWithEntities.replace(offset, offset + length, SPACE.repeat(length));
				});
		String clearedText = textWithEntities.toString().strip();
		LOGGER.debug("Cleared text: {}", clearedText);

		return clearedText;
	}

	public static String makeWebhookAnswerInlineQuery(long inlineQueryId, Map<String, String> resultMap) {
		List<Map<TelegramField, Object>> results = new ArrayList<>();
		Map<TelegramField, Object> message;
		Map<TelegramField, Object> result;

		for (Map.Entry<String, String> resultEntry : resultMap.entrySet()) {
			message = new HashMap<>();
			result = new HashMap<>();
			message.put(MessageText, resultEntry.getValue());
			message.put(ParseMode, PARSE_MODE_MARKDOWN_V2);
			result.put(Type, INLINE_QUERY_RESULT_ARTICLE);
			result.put(Id, getInlineResultId(resultEntry.getKey()));
			result.put(Title, resultEntry.getKey());
			result.put(InputMessageContent, message);
			result.put(ThumbLocation, CHAT_ICON);
			result.put(ThumbHeight, ICON_SIZE);
			result.put(ThumbWidth, ICON_SIZE);
			results.add(result);
		}
		return messageBuilder()
				.add(Method, ANSWER_INLINE_QUERY)
				.add(InlineQueryId, inlineQueryId)
				.add(Results, results)
				.add(IsPersonal, true)
				.build();
	}

	public static String makeWebhookAnswerInlineQuery(long inlineQueryId, @NotNull String title, @NotNull String markdownText) {
		return makeWebhookAnswerInlineQuery(inlineQueryId, title, markdownText, null);
	}

	public static String makeWebhookAnswerInlineQuery(long inlineQueryId, @NotNull String title, @NotNull String markdownText,
													  @Nullable String description) {
		Map<TelegramField, Object> message = new HashMap<>();
		Map<TelegramField, Object> result = new HashMap<>();

		message.put(MessageText, markdownText);
		message.put(ParseMode, PARSE_MODE_MARKDOWN_V2);
		result.put(Type, INLINE_QUERY_RESULT_ARTICLE);
		result.put(Id, EMPTY);
		result.put(Title, title);
		if (null != description) {
			result.put(Description, description);
			result.put(ThumbLocation, ERROR_ICON);
			result.put(ThumbHeight, ICON_SIZE);
			result.put(ThumbWidth, ICON_SIZE);
		} else {
			result.put(ThumbLocation, INFO_ICON);
			result.put(ThumbHeight, ICON_SIZE);
			result.put(ThumbWidth, ICON_SIZE);
		}
		result.put(InputMessageContent, message);

		return messageBuilder()
				.add(Method, ANSWER_INLINE_QUERY)
				.add(InlineQueryId, inlineQueryId)
				.add(Results, Collections.singletonList(result))
				.add(CacheTime, ONE_WEEK)
				.add(IsPersonal, true)
				.build();
	}

	public static String makeWebhookSendMessage(long chatId, String markdownText) {
		return messageBuilder()
				.add(Method, SEND_MESSAGE)
				.add(ChatID, chatId)
				.add(Text, markdownText)
				.add(DisableWebPagePreview, WEB_PAGE_PREVIEW_DISABLED)
				.add(ParseMode, PARSE_MODE_MARKDOWN_V2)
				.build();
	}

	public static String makeWebhookSendMessageWithPreview(long chatId, String markdownText) {
		return messageBuilder()
				.add(Method, SEND_MESSAGE)
				.add(ChatID, chatId)
				.add(Text, markdownText)
				.add(DisableWebPagePreview, WEB_PAGE_PREVIEW_ENABLED)
				.add(ParseMode, PARSE_MODE_MARKDOWN_V2)
				.build();
	}

	private static String getInlineResultId(String title) {
		String normalizedTitle = title.toLowerCase().replaceAll(REGEX_SPACE, BLANK);
		return normalizedTitle.substring(ZERO, Math.min(normalizedTitle.length(), MAX_ID_LENGTH));
	}

	private static MessageBuilder messageBuilder() {
		return new MessageBuilder();
	}

	private static class MessageBuilder {

		private static final String MESSAGE_FIELD_IS_NULL = "Message field cannot be null";

		private final Map<TelegramField, Object> fieldValues;

		MessageBuilder() {
			fieldValues = new HashMap<>();
		}

		public MessageBuilder add(TelegramField telegramField, Object value) {
			Objects.requireNonNull(telegramField, MESSAGE_FIELD_IS_NULL);

			fieldValues.put(telegramField, value);

			return this;
		}

		public String build() {
			return new JSONObject(fieldValues).toString();
		}

	}

}
