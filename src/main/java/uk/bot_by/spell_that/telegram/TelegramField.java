package uk.bot_by.spell_that.telegram;

public enum TelegramField {

	AnswerInlineQuery("answerInlineQuery"),
	CacheTime("cache_time"),
	ChatID("chat_id"),
	Description("description"),
	DisableNotification("disable_notification"),
	DisableWebPagePreview("disable_web_page_preview"),
	Id("id"),
	InlineQueryId("inline_query_id"),
	InputMessageContent("input_message_content"),
	IsPersonal("is_personal"),
	MessageText("message_text"),
	Method("method"),
	ParseMode("parse_mode"),
	ReplyToMessageID("reply_to_message_id"),
	Results("results"),
	Text("text"),
	Title("title"),
	ThumbHeight("thumb_height"),
	ThumbLocation("thumb_url"),
	ThumbWidth("thumb_width"),
	Type("type");

	private final String fieldName;

	TelegramField(String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public String toString() {
		return fieldName;
	}

}
