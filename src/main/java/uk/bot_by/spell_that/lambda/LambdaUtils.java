package uk.bot_by.spell_that.lambda;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

public class LambdaUtils {

	// Response
	private static final String APPLICATION_JSON = "application/json";
	private static final String CONTENT_TYPE = "Content-Type";
	private static final int HTTP_OK = 200;
	private static final Logger LOGGER = LoggerFactory.getLogger(LambdaUtils.class);
	private static final String OK = "OK";
	private static final String TEXT_PLAIN = "text/plain";

	public static APIGatewayProxyResponseEvent getResponseEvent(@NotNull Object body) {
		APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();

		LOGGER.trace("Response body: {}", body);
		responseEvent.setBody(body.toString());
		responseEvent.setHeaders(Collections.singletonMap(CONTENT_TYPE, APPLICATION_JSON));
		responseEvent.setIsBase64Encoded(false);
		responseEvent.setStatusCode(HTTP_OK);

		return responseEvent;
	}

	@NotNull
	public static APIGatewayProxyResponseEvent responseOK() {
		APIGatewayProxyResponseEvent responseEvent = getResponseEvent(OK);

		responseEvent.setHeaders(Collections.singletonMap(CONTENT_TYPE, TEXT_PLAIN));

		return responseEvent;
	}

}
