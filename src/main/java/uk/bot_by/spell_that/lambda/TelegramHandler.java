package uk.bot_by.spell_that.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.VisibleForTesting;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import uk.bot_by.spell_that.speller.SpellThatService;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static uk.bot_by.spell_that.lambda.LambdaUtils.getResponseEvent;
import static uk.bot_by.spell_that.telegram.TelegramUtils.clearSpelledText;
import static uk.bot_by.spell_that.telegram.TelegramUtils.makeWebhookAnswerInlineQuery;
import static uk.bot_by.spell_that.telegram.TelegramUtils.makeWebhookSendMessage;
import static uk.bot_by.spell_that.telegram.TelegramUtils.makeWebhookSendMessageWithPreview;

public class TelegramHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

	// Environment
	private static final String SPELLERS = "SPELLERS";
	// Logger
	private static final String AWS_REQUEST_ID = "AWSRequestId";
	private static final String EMPTY_REQUEST = "Empty request from {}";
	private static final String FORWARDED_FOR = "x-forwarded-for";
	private static final Logger LOGGER = LoggerFactory.getLogger(TelegramHandler.class);
	private static final String REQUEST_BODY = "Request body:\n{}";
	private static final String WRONG_REQUEST = "Wrong request from {}: {}\n{}";
	// Messages
	private static final String COULD_NOT_SPELL_HINT = "could-not-spell.hint";
	private static final String COULD_NOT_SPELL_MESSAGE = "could-not-spell.message";
	private static final String COULD_NOT_SPELL_TITLE = "could-not-spell.title";
	private static final String DEFAULT_LANGUAGE = "en";
	private static final String EMPTY_RESULT_MESSAGE = "empty-result.message";
	private static final String EMPTY_RESULT_TITLE = "empty-result.title";
	private static final String HELP_PAGE_MESSAGE = "[%s](%s)";
	private static final String HELP_PAGE_TITLE = "help-page.title";
	private static final String HELP_PAGE_URL = "help-page.url";
	private static final String MESSAGES_BUNDLE_NAME = "messages";
	private static final String WELCOME_TEXT = "welcome-message";
	// Telegram, update
	private static final String BOT_COMMAND = "bot_command";
	private static final String CHAT = "chat";
	private static final String ENTITIES = "entities";
	private static final String FROM = "from";
	private static final String ID = "id";
	private static final String INLINE_QUERY = "inline_query";
	private static final String LANGUAGE_CODE = "language_code";
	private static final String MESSAGE = "message";
	private static final String OFFSET = "offset";
	private static final String QUERY = "query";
	private static final String TEXT = "text";
	private static final String TYPE = "type";
	private static final String VIA_BOT = "via_bot";
	// Command
	private static final Function<JSONArray, Stream<JSONObject>> ENTITY_STREAM =
			(entities) -> StreamSupport.stream(entities.spliterator(), true)
					.map(object -> (JSONObject) object);
	private static final Predicate<Stream<JSONObject>> HAS_COMMANDS =
			(entityStream) -> entityStream
					.map(entity -> entity.get(TYPE))
					.anyMatch(BOT_COMMAND::equals);
	private static final Predicate<Stream<JSONObject>> ONLY_ONE_COMMAND =
			(entityStream) -> entityStream
					.map(entity -> entity.get(TYPE))
					.filter(BOT_COMMAND::equals)
					.count() == 1;
	private static final Pattern PATTERN_COMMAND_HELP = Pattern.compile("/help([?@])?.*");
	private static final Pattern PATTERN_COMMAND_SPELL = Pattern.compile("/spell([?@])?.*");
	private static final Pattern PATTERN_COMMAND_START = Pattern.compile("/start([?@])?.*");
	private static final Predicate<Stream<JSONObject>> START_WITH_COMMAND =
			(entityStream) -> entityStream
					.filter(entity -> 0 == (entity.getInt(OFFSET)))
					.anyMatch(entity -> BOT_COMMAND.equals(entity.getString(TYPE)));

	// Process text
	private static final String REGEX_SPACE = "\\s+";
	private static final String SPACE = " ";

	private final SpellThatService spellThatService;

	public TelegramHandler() {
		spellThatService = new SpellThatService(System.getenv(SPELLERS));
	}

	@Override
	public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent requestEvent, Context context) {
		Optional<APIGatewayProxyResponseEvent> responseEvent = Optional.empty();

		MDC.put(AWS_REQUEST_ID, context.getAwsRequestId());

		if (null == requestEvent.getBody() || requestEvent.getBody().isBlank()) {
			LOGGER.info(EMPTY_REQUEST, requestEvent.getHeaders().get(FORWARDED_FOR));
		} else {
			try {
				JSONObject update = new JSONObject(requestEvent.getBody());

				LOGGER.trace(REQUEST_BODY, update.toString());
				if (update.has(INLINE_QUERY)) {
					responseEvent = processInlineQuery(update.getJSONObject(INLINE_QUERY));
				} else if (update.has(MESSAGE) && update.getJSONObject(MESSAGE).has(TEXT)) {
					responseEvent = processMessage(update.getJSONObject(MESSAGE));
				} else {
					LOGGER.info("Unprocessed update: {}", update.keySet());
				}
			} catch (JSONException exception) {
				LOGGER.warn(WRONG_REQUEST, requestEvent.getHeaders().get(FORWARDED_FOR), exception.getMessage(),
						requestEvent.getBody());
			}
		}

		return responseEvent.orElseGet(LambdaUtils::responseOK);
	}

	@VisibleForTesting
	SpellThatService getSpellThatService() {
		return spellThatService;
	}

	@VisibleForTesting
	Optional<APIGatewayProxyResponseEvent> processInlineQuery(JSONObject message) {
		LOGGER.trace("Process inline query");

		long inlineQueryId = message.getLong(ID);
		ResourceBundle messages = ResourceBundle.getBundle(MESSAGES_BUNDLE_NAME,
				Locale.forLanguageTag(message.getJSONObject(FROM).optString(LANGUAGE_CODE, DEFAULT_LANGUAGE)));
		String query = message.getString(QUERY);

		if (query.isBlank()) {
			LOGGER.debug("Empty inline query");
			return Optional.of(getResponseEvent(makeWebhookAnswerInlineQuery(inlineQueryId,
					messages.getString(EMPTY_RESULT_TITLE), messages.getString(EMPTY_RESULT_MESSAGE))));
		}

		Map<String, String> spellingMap = new HashMap<>();
		List<String> spelling;

		for (String spellerName : getSpellThatService().getSpellerNames()) {
			spelling = getSpellThatService().spell(query, spellerName);
			if (spelling.isEmpty()) {
				LOGGER.debug("Speller {} has no spelling", spellerName);
				continue;
			}
			spellingMap.put(spellerName, String.join(SPACE, spelling));
		}
		if (spellingMap.isEmpty()) {
			LOGGER.debug("All spellers have no spellings");
			return Optional.of(getResponseEvent(makeWebhookAnswerInlineQuery(inlineQueryId,
					messages.getString(COULD_NOT_SPELL_TITLE), messages.getString(COULD_NOT_SPELL_MESSAGE),
					messages.getString(COULD_NOT_SPELL_HINT))));
		}

		return Optional.of(getResponseEvent(makeWebhookAnswerInlineQuery(inlineQueryId, spellingMap)));
	}

	@VisibleForTesting
	Optional<APIGatewayProxyResponseEvent> processMessage(JSONObject message) {
		LOGGER.trace("Process message");

		if (message.has(VIA_BOT)) {
			LOGGER.debug("Ignore message via another bot");
			return Optional.empty();
		}

		if (message.has(TEXT) && !message.getString(TEXT).isBlank()) {
			ResourceBundle messages = ResourceBundle.getBundle(MESSAGES_BUNDLE_NAME,
					Locale.forLanguageTag(message.getJSONObject(FROM).optString(LANGUAGE_CODE, DEFAULT_LANGUAGE)));
			String text = message.getString(TEXT);
			long chatId = message.getJSONObject(CHAT).getLong(ID);

			if (message.has(ENTITIES)) {
				JSONArray entities = message.getJSONArray(ENTITIES);

				if (HAS_COMMANDS.test(ENTITY_STREAM.apply(entities))) {
					LOGGER.debug("Process command(s)");

					if (START_WITH_COMMAND.test(ENTITY_STREAM.apply(entities))
							&& ONLY_ONE_COMMAND.test(ENTITY_STREAM.apply(entities))) {
						String[] commandTokens = text.split(REGEX_SPACE);
						String command = commandTokens[0].toLowerCase();

						if (1 == commandTokens.length) {
							if (PATTERN_COMMAND_HELP.matcher(command).matches()) {
								LOGGER.debug("Help command");

								return Optional.of(getResponseEvent(
										makeWebhookSendMessageWithPreview(chatId,
												String.format(HELP_PAGE_MESSAGE, messages.getString(HELP_PAGE_TITLE),
														messages.getString(HELP_PAGE_URL)))));
							} else if (PATTERN_COMMAND_START.matcher(command).matches()) {
								LOGGER.debug("Start command");

								return Optional.of(getResponseEvent(
										makeWebhookSendMessage(chatId, messages.getString(WELCOME_TEXT))));
							}
						} else if (PATTERN_COMMAND_SPELL.matcher(command).matches()) {
							LOGGER.debug("Spell command");

							return processText(messages, clearSpelledText(text, entities), chatId);
						}
					}
					// Others commands are ignored
					LOGGER.info("Command is ignored: {}", text);
				} else {
					LOGGER.trace("Spelling text with entities");

					return processText(messages, clearSpelledText(text, entities), chatId);
				}
			} else {
				LOGGER.trace("Spelling text");

				return processText(messages, text, chatId);
			}
		}

		return Optional.empty();
	}

	@NotNull
	private Optional<APIGatewayProxyResponseEvent> processText(ResourceBundle messages, String text, long chatId) {
		List<String> spelling = getSpellThatService().spell(text.replaceAll(REGEX_SPACE, SPACE));

		if (!spelling.isEmpty()) {
			return Optional.of(getResponseEvent(
					makeWebhookSendMessage(chatId, String.join(SPACE, spelling))));
		} else {
			LOGGER.debug("Speller returns empty result");
			return Optional.of(getResponseEvent(
					makeWebhookSendMessage(chatId, messages.getString(COULD_NOT_SPELL_MESSAGE))));
		}
	}

}
